# Confluence Page Restrictions UI Automated Tests

This is the source code for UI automated tests for page restrictions feature in Confluence. There are 4 tests available in the repo, which provide a good ground to contribute further by increasing automation coverage for more workflows/scenarios. 

Tests have been implemented using these tools:

- Page Object Model implemented for effective structure and easy maintenance of the code.
- Selenium driver, as the web framework.
- Ruby, as the programming language.
- Behaviour Driven Development (BDD) test scenarios, written in [Gherkin](https://cucumber.io/docs/gherkin/reference/) syntax, using [Cucumber](https://cucumber.io/docs/guides/overview/).

---

## How to run the tests?

Assumption: Chrome browser is installed on the machine

1. Clone the repo on your local machine
2. Terminal:  Navigate to the local folder where the repo has been cloned
3. Terminal:  ```rvm install 2.4.0```
4. Terminal:  ```gem install bundler```
5. Terminal:  ```bundle install```
6. Terminal:  ```cucumber features/page_permissions.feature```	

Assumption: Chrome is already installed on the machine.


---

## How to add new test data?

To keep the tests scalable and maintainable, all test data is coming from one source file "TestData/pagesAndAccounts.json". Follow the steps below to add new test data:

1. Add a new row in the file with a keyword. This keyword will be used in tests: ```"keyword"```
2. Put a colon after the keyword and then state the corresponding value for it: ```"value"```
3. Example of an entry in the file: ```"Username": "asma.gulbaz"```
4. Example of a test using this entry: ```When the user enters the username "Username"```


---

## How to add new webelements?

1. Each webpage is structured into a new page object file in features/pages folder. 
2. New webelement can be added at the top section of the featureName_page.rb in the following manner, to ensure the ID of the webelement is maintained in a single place:
```LOGIN_BUTTON = { id: 'loginButton'}```
3. Then, def modules will use LOGIN_BUTTON to access the login button web element.


---

## Constraints 

There had to be 2 places where inclusion of sleep seemed necessary. Inclusion of sleep is not ideal mostly. However, with more work and investigation, this can be overcome.  


- permissions_page.rb => get_icon_colour()
	The color of the restrictions icon changes from black to red when restrictions are put on the page. However, the webpage takes a second to reflect this change. Thus, it seemed useful to put 1 secs sleep on this step to increase stability of the test.   

		
- permissions_page.rb => enter_confluence_user(arg1)
    There is auto-search present in the permissions page to add a user in the restriction rules. There appears a "Searching..." progress wheel when we enter a user. This Searching... element has not been verified in the tests yet. However, with more work, it can be detected. For now, sleep 3 has been put in place.


