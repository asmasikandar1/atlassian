Feature: Page restrictions and permissions feature

  Scenario: As a user, I want to change restrictions on my page from no restrictions to no access for anyone, and get the red icon indication on the page
    Given the user launches a webpage "noRestrictionsPage"
    When the user enters the username "Username"
    Then the user enters the password "Password"
    And clicks on the login button
    Then verify that page title "No Restrictions Page" has launched
    #--cleanup: Reset the state of this page to Unrestricted before the test begins
    And confirm that permissions are reset on the page
    #--cleanup----
    Then click on the restrictions icon on the page
    Then choose a permissions option from the dropdown "Viewing Editing restricted"
    And click the apply button
    Then verify restrictions icon color is set to "red"
    Then logout from the user menu link

  Scenario: As a user, I should not be able to edit a page that has not given me editing permission
    Given the user launches a webpage "editingRestrictionsPage"
    When the user enters the username "Username"
    Then the user enters the password "Password"
    And clicks on the login button
    Then verify that page title "Ergonomics" has launched
    And verify that edit button doesnt appear
    Then click on the restrictions icon on the page
    Then verify that the editing restrictions error dialog appears
    Then cancel the restrictions page
    And logout from the user menu link

  Scenario: As a user, I should not be able to view a page that has restricted access for me
    Given the user launches a webpage "noAccessPage"
    When the user enters the username "Username"
    Then the user enters the password "Password"
    And clicks on the login button
    Then verify that there exists no access to this page
    And logout from the user menu link

  Scenario: change As a user, I can add a user to the page while others have no access
    Given the user launches a webpage "noRestrictionsPage"
    When the user enters the username "Username"
    Then the user enters the password "Password"
    And clicks on the login button
    Then click on the restrictions icon on the page
    Then choose a permissions option from the dropdown "Viewing Editing restricted"
    Then enter confluence user "nameOfUser" to add restrictions
    And click the add user button
    Then verify that the confluence user "nameOfUser" gets added
    And cancel the restrictions page
    Then logout from the user menu link
