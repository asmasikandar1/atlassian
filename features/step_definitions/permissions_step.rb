require_relative "../pages/permissions_page.rb"

Then(/^choose a permissions option from the dropdown "([^"]*)"$/) do |arg1|
  #@arg1: This argument can have these possible values:
  # "No restrictions", "Editing restricted", "Viewing Editing restricted"

  @permissions_page.choose_permission_option(arg1)
end

And(/^click the apply button$/) do
  @permissions_page.apply_button_click
end

Then(/^enter confluence user "([^"]*)" to add restrictions$/) do |arg1|
  #@arg1: This step is getting the Confluence user name from "NameOfUser" entry in pagesAndAccounts.json

  userData = File.open("TestData/pagesAndAccounts.json")
  confluenceUser = JSON.load userData
  @permissions_page.enter_confluence_user (confluenceUser[arg1])
end

And(/^click the add user button$/) do
  @permissions_page.add_user_button_click
end

Then(/^verify restrictions icon color is set to "([^"]*)"$/) do |arg1|
  #@arg1: This argument can have these possible values:
  # "red", "black"

  case arg1
  when "red"
    matching_color = @permissions_page.restrictions_icon_matching_color_red
  when "black"
    matching_color = @permissions_page.restrictions_icon_matching_color_black
  end

  if @permissions_page.get_icon_colour != matching_color
    p "Icon colour not set correctly - Failed"
    return false
  end
end

And(/^verify that the editing restrictions error dialog appears$/) do
  if @permissions_page.error_dialog_displayed
    p "Editing restrictions error dialog shown to the user - Passed"
  end
end

And(/^verify that the confluence user "([^"]*)" gets added$/) do |arg1|
  #@arg1: This step is getting the Confluence user name from "NameOfUser" entry in pagesAndAccounts.json

  userData = File.open("TestData/pagesAndAccounts.json")
  confluenceUser = JSON.load userData

  if @permissions_page.get_userlink (confluenceUser[arg1])
    p "User added and correct user link appeared - Passed"
  end
end

Then(/^cancel the restrictions page$/) do
  @permissions_page.cancel_button_click
end

#cleanup step to put the page in "No Restrictions" state
Then(/^confirm that permissions are reset on the page$/) do
  if @permissions_page.get_icon_colour != @permissions_page.restrictions_icon_matching_color_black
    @confluence_page.restrictions_icon_click
    @permissions_page.choose_permission_option("No restrictions")
    @permissions_page.apply_button_click
  end
end