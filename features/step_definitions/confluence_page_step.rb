require_relative "../pages/confluence_page.rb"

Then(/^verify that page title "([^"]*)" has launched$/) do |arg1|
  #@arg1: Title of the page

  if @confluence_page.title_page_value.include? arg1
    p "Correct page title displayed - Passed"
  else
    p "Incorrect page has launched - Failed"
    return false
  end
end

Then(/^click on the restrictions icon on the page$/) do
  @confluence_page.restrictions_icon_click
end

Then(/^logout from the user menu link$/) do
  @confluence_page.user_menu_link_click
  @confluence_page.logout_link_click
end

And(/^verify that edit button doesnt appear$/) do
  @confluence_page.edit_page_link_displayed
end