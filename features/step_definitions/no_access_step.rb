require_relative "../pages/no_access_page.rb"

And(/^verify that there exists no access to this page$/) do
  if @no_access_page.page_restricted_displayed
    p "No access given to the user for this page - Passed"
  end
end

