require_relative "../pages/login_page.rb"

Then(/^the user launches a webpage "([^"]*)"$/) do |arg1|
  #@arg1: This argument takes pages values from pagesAndAccounts.json, such as arg1 will serve as "arg1" entry in the file with corresponding value

  pageData = File.open("TestData/pagesAndAccounts.json")
  pageLink = JSON.load pageData
  @login_page.launch_login_page (pageLink[arg1])
end

Then(/^the user enters the username "([^"]*)"$/) do |arg1|
  #@arg1: This argument takes username values from pagesAndAccounts.json, such as arg1 will serve as "arg1" entry in the file with corresponding value

  usernameData = File.open("TestData/pagesAndAccounts.json")
  username = JSON.load usernameData
  @login_page.enter_username (username[arg1])
end

Then(/^the user enters the password "([^"]*)"$/) do |arg1|
  #@arg1: This argument takes password values from pagesAndAccounts.json, such as arg1 will serve as "arg1" entry in the file with corresponding value

  passwordData = File.open("TestData/pagesAndAccounts.json")
  password = JSON.load passwordData
  @login_page.enter_password (password[arg1])
end

Then(/^clicks on the login button$/) do
  @login_page.click_login_button
end



