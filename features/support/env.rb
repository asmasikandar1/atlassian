require 'selenium-webdriver'
require 'page-object'

Before do
  $driver = Selenium::WebDriver.for :chrome
  sleep 2 #wait for Chrome to get initialized

  $wait = Selenium::WebDriver::Wait.new(:timeout =>10)

  @login_page = LoginPage.new($driver)
  @confluence_page = ConfluencePage.new($driver)
  @permissions_page = PermissionsPage.new($driver)
  @no_access_page = NoAccessPage.new($driver)
end

After do
  $driver.quit
end