class NoAccessPage
  include PageObject

  PAGE_RESTRICTED = {id: "page-restricted"}

  def page_restricted_displayed
    return $wait.until{$driver.find_element(PAGE_RESTRICTED).displayed?}
  end

end #class end