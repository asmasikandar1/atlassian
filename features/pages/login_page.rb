class LoginPage
  include PageObject

  USER_NAME = { id: 'os_username'}
  PASSWORD = { id: 'os_password'}
  LOGIN_BUTTON = { id: 'loginButton'}

  def launch_login_page arg1
   $driver.get(arg1)
   $driver.manage.window.maximize()
  end

  def enter_username arg1
    $driver.find_element(USER_NAME).send_keys(arg1)
  end

  def enter_password arg1
    $driver.find_element(PASSWORD).send_keys(arg1)
  end

  def click_login_button
    $wait.until{$driver.find_element(LOGIN_BUTTON)}.click
  end

end #class end