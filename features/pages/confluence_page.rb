class ConfluencePage
  include PageObject

  PAGE_TITLE = {id: 'title-text'}
  RESTRICTIONS_ICON = {id: 'content-metadata-page-restrictions'}
  USER_MENU_LINK = {id: "user-menu-link"}
  LOGOUT_LINK = {id: "logout-link"}
  EDIT_PAGE_LINK = {id: "editPageLink"}

  def restrictions_icon_click
    $driver.find_element(RESTRICTIONS_ICON).click
  end

  def title_page_value
    return $wait.until{$driver.find_element(PAGE_TITLE).text}
  end

  def user_menu_link_click
    $driver.find_element(USER_MENU_LINK).click
  end

  def logout_link_click
    $driver.find_element(LOGOUT_LINK).click
  end

  def edit_page_link_displayed
    begin
      $driver.find_element(EDIT_PAGE_LINK)
      p "Edit button found - Failed: Did not restrict editing correctly"
      return false
    rescue Selenium::WebDriver::Error::NoSuchElementError
      p "Edit button not found - Passed"
    end
  end

end #class end