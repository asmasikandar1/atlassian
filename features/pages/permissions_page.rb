class PermissionsPage
  include PageObject

  ARROW = {id: "s2id_page-restrictions-dialog-selector"}
  DROPDOWN = {id: "page-restrictions-dialog-selector"}
  PERMISSION_CANVIEW = {class: "no-select"}
  RESTRICTIONS_TEXT = {link_text: 'Restrictions apply'}
  RESTRICTIONS_ICON = {id: "content-metadata-page-restrictions"}
  ERROR_DIALOG = {id: "page-restrictions-dialog-error-div"}
  ENTER_CONFLUENCE_USER = {id: "s2id_autogen2"}
  APPLY_BUTTON = {id: "page-restrictions-dialog-save-button"}
  CANCEL_BUTTON = {id: "page-restrictions-dialog-close-button"}
  ADD_USER_BUTTON = {id: "page-restrictions-add-button"}

  #Expected values on the page
  RESTRICTIONS_ICON_EXPECTED_RED_COLOUR = "rgba(208, 68, 55, 1)"
  RESTRICTIONS_ICON_EXPECTED_BLACK_COLOUR = "rgba(66, 82, 110, 1)"
  RESTRICTIONS_ICON_RESTRICTIONS_APPLY_EXPECTED_TEXT = "Restrictions apply"

  #Option values
  NO_RESTRICTIONS = "none"
  EDITING_RESTRICTED = "edit"
  VIEWING_EDITING_RESTRICTED= "viewedit"

  def restrictions_icon_matching_color_red
    return RESTRICTIONS_ICON_EXPECTED_RED_COLOUR
  end

  def restrictions_icon_matching_color_black
    return RESTRICTIONS_ICON_EXPECTED_BLACK_COLOUR
  end

  def restrictions_icon_restrictions_apply_matching_text
    return RESTRICTIONS_ICON_RESTRICTIONS_APPLY_EXPECTED_TEXT
  end

  def cancel_button_click
    $wait.until{$driver.find_element(CANCEL_BUTTON)}.click
  end

  def choose_permission_option arg1
    #@arg1: This argument can have these possible values:
    # "No restrictions", "Editing restricted", "Viewing Editing restricted"

    case arg1
    when "No restrictions"
      requiredOption=NO_RESTRICTIONS
    when "Editing restricted"
      requiredOption=EDITING_RESTRICTED
    when "Viewing Editing restricted"
      requiredOption=VIEWING_EDITING_RESTRICTED
    end

    if $wait.until{$driver.find_element(DROPDOWN).displayed?}
      select = $driver.find_element(DROPDOWN)
      allOptions = select.find_elements(:tag_name, "option")

      allOptions.each do |option|
        if option.attribute("value") == requiredOption
          option.click
          break
        end
      end
    end
  end

  def apply_button_click
    $driver.find_element(APPLY_BUTTON).click
  end

  def get_icon_colour
    #This wait is important for the page to have the new color to come in effect
    sleep 1
    return $driver.find_element(RESTRICTIONS_ICON).css_value("color")
  end

  def error_dialog_displayed
    return $wait.until{$driver.find_element(ERROR_DIALOG).displayed?}
  end

  def enter_confluence_user arg1
    #@arg1 : Name of the Confluence user to enter on the permissions page

    $driver.find_element(ENTER_CONFLUENCE_USER).send_keys(arg1)
    sleep 3   # searching for user
    $driver.find_element(ENTER_CONFLUENCE_USER).send_keys(:return)
  end

  def add_user_button_click
    $driver.find_element(ADD_USER_BUTTON).click
  end

  def get_userlink arg1
    #@arg1 : Name of the Confluence user to verify being added on the permissions page

    return $wait.until{$driver.find_element(:link_text, arg1).displayed?}
  end

end #class end